syntax enable
set background=dark
set number
set showmatch
set wrap
set hlsearch
set incsearch
set expandtab
set shiftwidth=2
set softtabstop=2
autocmd BufWritePre * %s/\s\+$//e
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/

